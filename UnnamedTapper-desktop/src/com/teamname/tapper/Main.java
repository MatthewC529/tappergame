package com.teamname.tapper;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.oogames.tapper.Ref;
import com.oogames.tapper.Tapper;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = Ref.TITLE + " " + Ref.VERSION;
		cfg.useGL30 = false;
		cfg.width = 480;
		cfg.height = 800;
		
		new LwjglApplication(new Tapper(), cfg);
	}
}
