package com.oogames.tapper.handler;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.treehouseelite.elitegdx.Logger;
import com.treehouseelite.elitegdx.util.collections.ArrayUtils;

public class TextHandler implements Disposable{

	private ArrayMap<BitmapFont, ScreenTextItem[]> items;
	
	public TextHandler(){
		items = new ArrayMap<BitmapFont, ScreenTextItem[]>(BitmapFont.class, ScreenTextItem[].class);
	}
	
	public void addScreenText(BitmapFont font, String str, float x, float y){
		if(items.containsKey(font)){
			insertValueAt(font, new ScreenTextItem(str, x, y));
		}
		
		items.put(font, new ScreenTextItem[]{new ScreenTextItem(str, x, y)});
	}
	
	public void addScreenText(BitmapFont font, String str, String name, float x, float y){
		if(items.containsKey(font)){
			insertValueAt(font, new ScreenTextItem(str, name, x, y));
		}
		
		items.put(font, new ScreenTextItem[]{new ScreenTextItem(str, name, x, y)});
	}
	
	public void removeScreenText(String str, String name){
		ScreenTextItem text;
		
		if(name == null || name.isEmpty()){
			text = new ScreenTextItem(str, 0, 0);
		}else{
			text = new ScreenTextItem(str, name, 0, 0);
		}
		
		for(ScreenTextItem[] x : items.values){
			for(int i = 0 ; i < x.length ; i++){
				if(x[i].equals(text)){
					BitmapFont key = items.getKey(x, true);
					x[i] = null;
					x = ArrayUtils.condenseArray(x);
					items.put(key, x);
					return;
				}
			}
		}
		
	}
	
	public void removeScreenText(String text){
		removeScreenText(text, null);
	}
	
	public void removeScreenText(BitmapFont font, int index){
		ScreenTextItem[] items = this.items.get(font);
		
		try{
			items[index] = null;
			items = ArrayUtils.condenseArray(items);
			this.items.put(font, items);
		}catch(ArrayIndexOutOfBoundsException e){
			Logger.warn("Failure to Remove Screen Text Due to ArrayIndexOutOfBounds Error! -- " + e.getStackTrace()[0]);
		}
	}
	
	public void removeScreenText(BitmapFont font, String str, String name){
		
		ScreenTextItem text;
		ScreenTextItem[] x = items.get(font);
		
		if(name == null || name.isEmpty()){
			text = new ScreenTextItem(str, 0, 0);
		}else{
			text = new ScreenTextItem(str, name, 0, 0);
		}
		
		for(int i = 0 ; i < x.length ; i++){
			if(x[i].equals(text)){
				x[i] = null;
				x = ArrayUtils.condenseArray(x);
				items.put(font, x);
				return;
			}
		}
		
	}
	
	public void renderItems(SpriteBatch batch){
		BitmapFont[] keys = items.keys;
		
		for(BitmapFont font: keys){
			ScreenTextItem[] texts = items.get(font);
			for(ScreenTextItem stext: texts){
				font.draw(batch, stext.text, stext.x, stext.y);
			}
		}
		
	}
	
	private void insertValueAt(BitmapFont font, ScreenTextItem value){
		ScreenTextItem[] arr = items.get(font);
		arr = ArrayUtils.resizeArray(arr, arr.length+1);
		arr[arr.length-1] = value;
		items.put(font, arr);
	}
	
	public void dispose(){
		if(items.size <= 0) return;
		for(BitmapFont font: items.keys){
			font.dispose();
		}
		
		items.clear();
	}
	
}

class ScreenTextItem{
	
	public final String text;
	public final String name;
	public final float x, y;
	
	public ScreenTextItem(String str, float x, float y){
		text = str;
		name = "";
		this.x = x;
		this.y = y;
	}
	
	public ScreenTextItem(String str, String name, float x, float y){
		text = str;
		this.name = name;
		this.x = x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof ScreenTextItem){
			if(name.equals(((ScreenTextItem) o).name) && name != null && !name.isEmpty()) return true;
			if(text.equals(((ScreenTextItem) o).text)) return true;
		}
		
		return false;
	}
	
}
