package com.oogames.tapper.exception;

/**
 * Some Kind of Asset is unavailable and the Object can continue but must turn to a pre-set <br />
 * state instead of the expected state. <br />
 * <br />
 * In some cases this may lead to a {@link NonExistantAssetException} later down the line.
 * 
 * @author Matthew Crocco
 */
public class AssetException extends RuntimeException {

	public AssetException() {
		super();
	}

	public AssetException(String message) {
		super(message);
	}

	public AssetException(Throwable cause) {
		super(cause);
	}

	public AssetException(String message, Throwable cause) {
		super(message, cause);
	}

}
