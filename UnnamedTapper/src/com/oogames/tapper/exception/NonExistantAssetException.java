package com.oogames.tapper.exception;

/**
 * Indicates that an Asset needed to initialize or use an object is not available <br />
 * Said object can not continue in a valid manner and must stop execution of the current call.
 * 
 * @author Matthew Crocco
 */
public class NonExistantAssetException extends Exception {

	public NonExistantAssetException() {
		super();
	}

	public NonExistantAssetException(String message) {
		super(message);
	}

	public NonExistantAssetException(Throwable cause) {
		super(cause);
	}

	public NonExistantAssetException(String message, Throwable cause) {
		super(message, cause);
	}

}
