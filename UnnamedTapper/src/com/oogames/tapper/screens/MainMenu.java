package com.oogames.tapper.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteCache;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.oogames.tapper.Assets;
import com.oogames.tapper.Ref;
import com.oogames.tapper.entity.Cubit;
import com.oogames.tapper.entity.MenuItem;
import com.oogames.tapper.handler.TextHandler;
import com.oogames.tapper.util.ScreenAdapter;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.math.Interpolate;
import com.treehouseelite.elitegdx.util.Colour;
import com.treehouseelite.elitegdx.util.Utility;

public class MainMenu extends ScreenAdapter{

	private Game game;
	private SpriteBatch batch;
	private SpriteCache cache;
	private TextHandler text;
	private Texture splash;
	private Music music;
	private MenuItem title, rush, infinity, master, instructions;
	private Viewport viewport;
	private Timer timer;
	private Colour bg;
	
	private float alpha;
	private int cacheId;
	
	private float fadeFactor = 0.08f;
	private float alphaIncrements = 0.025f;
	private boolean fadeOut = false;
	private boolean adjustCamera = false;
	
	public MainMenu(Game game){
		this.game = game;
		viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		batch = new SpriteBatch();
		cache = new SpriteCache();
		text = new TextHandler();
		bg = Ref.BG_COLOR;
		
		//-- Assets to Load for This Instance
		Manager.addSingletonFinalizer(Cubit.class, "disposeAndClear");
		Assets.load(Ref.TITLE_TEX, Texture.class);
		Assets.load(Ref.ITEM1_TEX, Texture.class);
		Assets.load(Ref.ITEM2_TEX, Texture.class);
		Assets.load(Ref.ITEM3_TEX, Texture.class);
		Assets.load(Ref.ITEM4_TEX, Texture.class);
		Assets.load(Ref.CUBIT_TEX, Texture.class);
		Assets.load(Ref.SMALL_CUBIT_TEX, Texture.class);
		Assets.load(Ref.MAIN_FNT, BitmapFont.class);
		Assets.load(Ref.TITLE_FNT, BitmapFont.class);
		splash = Assets.get("data/splash.png");
		music = Assets.get(Ref.TITLE_MUSIC);
		music.setLooping(true);
		music.setVolume(0);
		//--
		
		timer = new Timer();
		
		alpha = 1.0f;
		Manager.getLogger().info("Initialized Main Menu!");
	}

	@Override
	public void render(final float delta) {
		Gdx.gl.glClearColor(bg.r, bg.g, bg.b, bg.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		
		if(rush == null && !assetsLoaded()){
			batch.begin();
			batch.draw(splash, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			batch.end();
			return;
		}else if(alpha >= 0 && !fadeOut){
			if(!music.isPlaying())
				music.play();
			float lerped = Interpolate.linear.apply(alpha, 0, fadeFactor);
			music.setVolume(1 - lerped);
			batch.setColor(1.0f, 1.0f, 1.0f, lerped);
			batch.begin();
			batch.draw(splash, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			batch.end();
			alpha -= alphaIncrements;
			if(alpha <= 0)
				fadeOut = true;
			return;
		}
		
		//Viewport Centers Camera, This adjusts it initially to center on the content
		if(!adjustCamera){
			timer.schedule(new Task() {
				public void run() {
					Cubit.getCubit(viewport);
				}
			}, 0.5f, 0.2f);
			viewport.getCamera().translate(viewport.getViewportWidth()/2, viewport.getViewportHeight()/2, 0);
			adjustCamera = true;
		}
		
		updateItems(Math.min(delta, 1/30));
		
		cache.begin();
		cache.draw(cacheId);
		cache.end();
		
		batch.setProjectionMatrix(viewport.getCamera().combined);
		batch.setColor(1.0f, 1.0f, 1.0f, alpha >= 1.0f ? 1.0f : Interpolate.linear.apply(alpha, 1.0f, fadeFactor));
		batch.begin();
		drawCubits(batch);
		rush.draw(batch);
		title.draw(batch);
		infinity.draw(batch);
		master.draw(batch);
		instructions.draw(batch);
		batch.end();
		
		if(alpha != 1.0f){
			alpha += alphaIncrements;
		}
	}
	
	/**
	 * Updates Specified Items based on the Frame Delta
	 * @param delta
	 */
	private void updateItems(float delta){
		for(Cubit c: Cubit.getAllLiveCubits())
			c.update(delta);
		title.update(delta);
		rush.update(delta);
		infinity.update(delta);
		master.update(delta);
		instructions.update(delta);
		viewport.update();
		viewport.getCamera().update();
	}
	
	private void drawCubits(SpriteBatch batch){
		for(Cubit c: Cubit.getAllLiveCubits()){
			c.draw(batch);
		}
	}
	
	/**
	 * Returns whether or not the AssetManager has finished loading our assets. <br />
	 * <br />
	 * If it has finished then it will get the assets and modify the parameters as necessary!
	 * @return
	 */
	private boolean assetsLoaded(){
		if(!Assets.update())
			return false;
		
		Texture titleTex = Assets.get(Ref.TITLE_TEX);		//Title Texture
		Texture item = Assets.get(Ref.ITEM1_TEX);			//MenuItem Texture for Benchmarking
		BitmapFont fnt = Assets.get(Ref.MAIN_FNT);			//Standard Menu Font
		BitmapFont titleFnt = Assets.get(Ref.TITLE_FNT);	//Larger Title Font
		
		float mih = item.getHeight();					//MenuItem Height
		float miw = item.getWidth();					//MenuItem Width
		
		float width = Gdx.graphics.getWidth();			//Screen Width
		float height = Gdx.graphics.getHeight();		//Screen Height
		float pRatio = width / Ref.PREF_WIDTH_PX;		//Screen Width / Benchmark
		float hRatio = height / Ref.PREF_HEIGHT_PX;		//Screen Height / Benchmark
		
		//--UNUSED
		float center = (width/2) - (titleTex.getWidth()/2);			//Center X of Screen
		float bmh = height - titleTex.getHeight() - (20 * pRatio); 	//Benchmark Y Position
		//--UNUSED
		
		float icw = (width/2) - (miw/2);				//MenuItem X Center
		
		/*
		 * Very Readable Code Below!
		 * 
		 * Title is placed in the Top Right Corner
		 * 
		 * Menu Items are placed in the center to be stylistically off of the Title.
		 * 
		 * pRatio = ScreenWidth / BenchmarkWidth = Proportional Difference in Width
		 * hRatio = ScreenHeight / BenchmarkHeight = Proportional Difference in Height
		 * icw = (ScreenWidth/2) - (MenuItemWidth/2) = Center Of Screen
		 * bmh = ScreenHeight - TitleTextureHeight - (20 * RatioToBenchmark) = Arbitrary Benchmark Y Position
		 *
		 * Non-Title Width = Centered
		 * Non-Title Height = Height of Title - (standard texture height * itemNumber) - (ArbitraryValue * heightRatio)
		 */
		title = new MenuItem(width - titleTex.getWidth(), height - titleTex.getHeight(), titleTex, "Tapper", titleFnt, MenuItemAction.DO_NOTHING);
		rush = new MenuItem(icw, bmh - mih - (30 * hRatio), Assets.<Texture>get("data/mainmenu/mItem1.png"), "Rush", fnt, MenuItemAction.DO_NOTHING);
		infinity = new MenuItem(icw, bmh - (mih * 2) - (50 * hRatio), Assets.<Texture>get("data/mainmenu/mItem2.png"), "Infinity" , fnt, MenuItemAction.DO_NOTHING);
		master = new MenuItem(icw, bmh - (mih * 3) - (70 * hRatio), Assets.<Texture>get("data/mainmenu/mItem3.png"), "Master", fnt, MenuItemAction.DO_NOTHING);
		instructions = new MenuItem(icw, bmh - (mih * 4) - (90 * hRatio), Assets.<Texture>get("data/mainmenu/mItem4.png"), "Instructions", fnt, MenuItemAction.DO_NOTHING);
		
		cache.beginCache();
		//BACKGROUND
		cacheId = cache.endCache();
		
		return true;
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void dispose() {
		Manager.runCleanUp();		//Does Not Affect Singletons
		Utility.collectGarbage();
		unloadAssets();
		
		Cubit.disposeAndClear();
		text.dispose();
		batch.dispose();
		cache.dispose();
	}
	
	/**
	 * Unloads/Disposes Assets Not Needed after this menu. <br />
	 * This may be changed as Number of Assets increases and Load Time increase or <br />
	 * if it is faster to instead keep all assets loaded and only load them as necessary.
	 */
	private void unloadAssets(){
		Assets.unload(Ref.TITLE_TEX);
		Assets.unload(Ref.ITEM1_TEX);
		Assets.unload(Ref.ITEM2_TEX);
		Assets.unload(Ref.ITEM3_TEX);
		Assets.unload(Ref.ITEM4_TEX);
	}
	
	
}
