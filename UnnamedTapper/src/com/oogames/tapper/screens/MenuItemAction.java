package com.oogames.tapper.screens;

public abstract class MenuItemAction implements Runnable{
	
	public abstract void run();
	
	public static final MenuItemAction DO_NOTHING = new MenuItemAction() {
		@Override
		public void run() {
		}
	};

}
