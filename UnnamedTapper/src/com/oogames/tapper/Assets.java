package com.oogames.tapper;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.oogames.tapper.exception.AssetException;
import com.treehouseelite.elitegdx.Manager;

/**
 * 
 * A Manager Class to allow for a universal Asset Manager in as simple a way as possible. <br />
 * <br />
 * {@link AssetManager}
 * 
 * @author Matthew Crocco
 */
public final class Assets {

	private static AssetManager manager;
	private static Array<String> assetsLoaded = new Array<>();
	
	protected static final void init(){
		manager = new AssetManager();
		Assets.load("data/splash.png", Texture.class);
		Assets.load(Ref.TITLE_MUSIC, Music.class);
		Assets.finishLoading();	//Ensure the Splash Screen is Loaded, it should NEVER be Disposed
		Manager.addSingletonFinalizer(Assets.class, "dispose");
	}
	
	public static final boolean isLoaded(String asset){
		if(!assetsLoaded.contains(asset, false)){
			Manager.getLogger().warn("The Asset " + asset + " is NOT Registered and therefore, Not Loaded!");
			return false;
		}
		return manager.isLoaded(asset);
	}
	
	/**
	 * See the {@link AssetManager#load(String, Class) AssetManager Load} method. <br />
	 * <br />
	 * If the asset is already loaded then the method notifies the Logger and then return. <br />
	 * In that case the AssetManager will NOT be called.
	 * @param asset
	 * @param type
	 */
	public static final void load(String asset, Class<?> type){
		if(assetsLoaded.contains(asset, false)) return;
		manager.load(asset, type);
		assetsLoaded.add(asset);
	}
	
	/**
	 * {@link AssetManager#update()}
	 * @return
	 */
	public static final boolean update(){
		boolean update = manager.update();
		Manager.getLogger().info(String.format("Loading Progress: %s%%", manager.getProgress() * 100));
		return update;
	}
	
	/**
	 * {@link AssetManager#finishLoading()}
	 */
	public static final void finishLoading(){
		Manager.getLogger().info("Hanging program to load assets...");
		manager.finishLoading();
		Manager.getLogger().info("Assets Loaded! Continuing...");
	}
	
	/**
	 * {@link AssetManager#getProgress()}
	 * @return
	 */
	public static final float getProgress(){
		return manager.getProgress();
	}
	
	/**
	 * #{@link AssetManager#get(String)}
	 * @param asset
	 * @return
	 */
	public static final <T> T get(String asset){
		if(manager.isLoaded(asset)){
			return manager.get(asset);
		}else throw new AssetException("Asset Not Yet Loaded! : " + asset);
	}
	
	/**
	 * {@link AssetManager#unload(String)} <br />
	 * <br />
	 * If the asset is not loaded, this method will notify the Logger and then return. <br />
	 * Meaning the AssetManager will not be called.
	 * @param asset
	 */
	public static final void unload(String asset){
		if(!assetsLoaded.contains(asset, false)){
			Manager.getLogger().warn("Attempt to Unload a Non-Existant Asset!");
			return;
		}
		manager.unload(asset);
		assetsLoaded.removeValue(asset, false);
	}
	
	public static final String[] getCurrentlyRegisteredAssets(){
		return assetsLoaded.toArray(String.class);
	}
	
	/**
	 * Unloads all Loaded Assets
	 */
	public static final void unloadAll(){
		for(String asset: assetsLoaded){
			manager.unload(asset);
		}
		
		assetsLoaded.clear();
	}
	
	/**
	 * Disposes the Current AssetManager. ONLY USE THIS AT GAME TERMINATION!
	 */
	public static final void dispose(){
		unloadAll();
		manager.dispose();
		Manager.getLogger().info("Asset Loader Disposed Successfully...");
	}
	
}
