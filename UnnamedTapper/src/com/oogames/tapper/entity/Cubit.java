package com.oogames.tapper.entity;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.oogames.tapper.Assets;
import com.oogames.tapper.Ref;
import com.oogames.tapper.exception.NonExistantAssetException;
import com.oogames.tapper.util.Utils;
import com.treehouseelite.elitegdx.math.Interpolate;


//TODO Re-Implement Manager. Give Individual Textures or do not Dispose Texture on Disposal, simply remove Reference in Cubits Array
/**
 * A Quick Moving Square that zips across the Background, acting as a distractor and as <br />
 * busy work for the background. <br />
 * <br />
 * All Cubits share one Texture provided by the AssetManager. <br />
 * Cubits are also Non-SimpleEntity Entities, and are NOT handled by EliteGDX.
 * @author Matthew Crocco
 */
public class Cubit{

	private static final Interpolate lerp = Interpolate.linear;		//Linear Interpolator
	private static final Random r = new Random();					//PRNG
	private static final float _a = 0.025f;							//Alpha Value for the Cubits to Move At TODO: Randomize
	private static final int largeValue = 200;						//Arbitrarily Large Value 
	private static final Array<Cubit> cubits = new Array<Cubit>();
	
	private Vector2 start, end;		//Start/Current and End Positions
	private float alphaFactor;		//Velocity of the Cubit
	private Sprite sprite;			//Cubit Sprite
	
	private Cubit(float x, float y, float dx, float dy, float alpha, boolean small){
		start = new Vector2(x, y);
		end = new Vector2(dx, dy);
		if(!small)
			sprite = new Sprite(Assets.<Texture>get(Ref.CUBIT_TEX));
		else
			sprite = new Sprite(Assets.<Texture>get(Ref.SMALL_CUBIT_TEX));
		sprite.setPosition(x, y);
		alphaFactor = alpha;
	}
	
	/**
	 * Generates a Random Cubit within the given Viewport.
	 * @param viewport
	 * @return
	 * @throws NonExistantAssetException
	 */
	public static final Cubit getCubit(Viewport viewport) {
		r.setSeed(System.currentTimeMillis());
		Cubit c;
		
		int xOffset = (int) (Utils.propWidth(largeValue, viewport));
		int yOffset = (int) (Utils.propHeight(largeValue, viewport));
		int maxX = viewport.getViewportWidth() + xOffset;
		int maxY = viewport.getViewportHeight() + yOffset;
		int minX = 0 - xOffset;
		int minY = 0 - yOffset;
		int orient = (int)(r.nextInt(10000) + 1 * r.nextGaussian());
		int val;
		
		if(orient >= 5000){			//Moving Right or Left
			val = r.nextInt(maxY);
			if(orient >= 7500){		//Moving Right
				c = new Cubit(minX, val, maxX, val, _a, r.nextBoolean());
			}else{					//Moving Left
				c = new Cubit(maxX, val, minX, val, _a, r.nextBoolean());
			}
		}else{						//Moving Up or Down
			val = r.nextInt(maxX);
			if(orient >= 2500){		//Moving Up
				c = new Cubit(val, minY, val, maxY, _a, r.nextBoolean());
			}else{					//Moving Down
				c = new Cubit(val, maxY, val, minY, _a, r.nextBoolean());
			}
		}
		
		cubits.add(c);
		return c;
	}
	
	public static final Array<Cubit> getAllLiveCubits(){
		return cubits;
	}
	
	public static final void disposeAndClear(){
		cubits.clear();
	}

	public void update(float delta) {
		float cX = start.x, cY = start.y;
		float dX = end.x, dY = end.y;
		
		cX = lerp.apply(cX, dX, alphaFactor);	//TODO may cause issues down the line if dX > cX
		cY = lerp.apply(cY, dY, alphaFactor);	//TODO may cause issues down the line if dY > cY
		
		start.set(cX, cY);
		
		sprite.setPosition(cX, cY);
		
		if(new Vector2(cX, cY).epsilonEquals(dX, dY, 60.0f)){
			cubits.removeValue(this, true);
		}
	}
	
	public void draw(SpriteBatch batch){
		sprite.draw(batch);
	}

}
