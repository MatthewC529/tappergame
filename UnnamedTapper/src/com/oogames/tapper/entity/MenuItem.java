package com.oogames.tapper.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.oogames.tapper.screens.MenuItemAction;
import com.treehouseelite.elitegdx.entity.SimpleEntity;
import com.treehouseelite.elitegdx.util.Utility;

public class MenuItem extends SimpleEntity{

	private Runnable action;
	
	private BitmapFont font;
	private String text;
	
	float tX, tY;
	
	public MenuItem(float x, float y, Texture tex, String text, BitmapFont font, MenuItemAction r) {
		super(x, y, tex);
		this.text = text;
		this.font = font;
		this.tX = getSprite().getX() + (getSprite().getWidth()/2) - (font.getBounds(text).width/2);
		this.tY = getSprite().getY() + (getSprite().getHeight()/2) + (font.getBounds(text).height/2);
		action = r;
	}

	@Override
	public void update(float delta) {
		Vector2 inputPos = Utility.getMousePosition();
		
		this.tX = getSprite().getX() + (getSprite().getWidth()/2) - (font.getBounds(text).width/2);
		this.tY = getSprite().getY() + (getSprite().getHeight()/2) + (font.getBounds(text).height/2);
		
		if(this.getCollisionShape().getShape().contains(inputPos)){
			action.run();
		}
		this.updateMovement();
	}
	
	@Override
	public void draw(SpriteBatch batch){
		super.draw(batch);
		font.draw(batch, text, tX, tY);
	}

	@Override
	public void onFinish() {
		
	}

	@Override
	public void sleep() {
	}

	@Override
	public void wakeUp() {
	}

	@Override
	public boolean isSleeping() {
		return false;
	}
	
}
