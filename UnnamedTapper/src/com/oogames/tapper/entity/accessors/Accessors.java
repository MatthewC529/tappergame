package com.oogames.tapper.entity.accessors;

import java.lang.reflect.Field;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;

import com.treehouseelite.elitegdx.Manager;

public final class Accessors {

	private Accessors(){}
	
	/* TWEEN TYPES:
	 * 
	 * X = X Position
	 * Y = Y Position
	 * XY = X, Y Position
	 * R = Radius
	 * D = Diameter
	 * A = Arbitrary*/
	public static final int X = 1, Y = 2, XY = 3, R = 4, D = 5, A = 6;
	
	//Define All Accessors as acronymAccessor
	//i.e. MenuItem's acronym is MI so the accessor as a field is
	//	   miAccessor as seen below.
	private static final MenuItemAccessor miAccessor = new MenuItemAccessor();
	private static final PointCircleAccessor pcAccessor = new PointCircleAccessor();
	
	/**
	 * Grabs all registered fields by Reflection and registers them with Tween*/
	public static void init(){
		Field[] fields = Accessors.class.getDeclaredFields();
		
		for(Field f: fields){
			if(f.getName().contains("Accessor")){
				try{
					Tween.registerAccessor(f.getClass(), (TweenAccessor<?>)f.get(null));
					Manager.getLogger().log("Successfully registered Accessor: " + f.getName());
				}catch(Exception e){
					Manager.getLogger().warn("Failure to register the Accessor: " + f.getName() +" -- Some Tweens May Fail!"); 
				}
			}
		}
	}
	
}
