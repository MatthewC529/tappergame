package com.oogames.tapper.entity.accessors;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.math.Vector2;
import com.oogames.tapper.entity.MenuItem;

public class MenuItemAccessor implements TweenAccessor<MenuItem>{
	@Override
	public int getValues(MenuItem target, int tweenType, float[] returnValues) {
		switch(tweenType){
		case Accessors.X: returnValues[0] = target.getCurPos().x; return 1;
		case Accessors.Y: returnValues[0] = target.getCurPos().y; return 1;
		case Accessors.XY: returnValues[0] = target.getCurPos().x; returnValues[1] = target.getCurPos().y; return 2;
		default: return 0;
		}
	}

	@Override
	public void setValues(MenuItem target, int tweenType, float[] newValues) {
		Vector2 original = target.getCurPos();
		
		switch(tweenType){
		case Accessors.X:
			original.x = newValues[0];
			break;
		case Accessors.Y:
			original.y = newValues[0];
			break;
		case Accessors.XY:
			original.x = newValues[0];
			original.y = newValues[1];
			break;
		default:
			break;
		}
		
		target.setCurPos(original);
	}

}
