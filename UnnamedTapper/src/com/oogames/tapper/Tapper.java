package com.oogames.tapper;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.oogames.tapper.entity.accessors.Accessors;
import com.oogames.tapper.screens.MainMenu;
import com.treehouseelite.elitegdx.GameBuilder;
import com.treehouseelite.elitegdx.Manager;
import com.treehouseelite.elitegdx.util.Referenceable;
import com.treehouseelite.elitegdx.util.Utility;
import com.treehouseelite.elitegdx.util.event.ProteusTask;

/**
 * Main Entry Point for Program
 * 
 * @author Matthew
 */
public class Tapper extends Game {
	
	@Override
	public void create() {
		GameBuilder gb = new GameBuilder();
		gb.initialize(Utility.<Referenceable>getHiddenObject(Ref.class), this);
		Accessors.init();
		Assets.init();
		Manager.getLogger().info("Initialization of Game Complete!");
		this.setScreen(new MainMenu(this));
		Gdx.app.addLifecycleListener(new ProteusTask() {
			@Override
			public void dispose(){
				Manager.finishUp();
			}
		});
	}
	
}
