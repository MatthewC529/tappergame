package com.oogames.tapper.util;

import com.badlogic.gdx.Screen;

public abstract class ScreenAdapter implements Screen{

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
		
	}

	
	
}
