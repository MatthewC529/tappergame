package com.oogames.tapper.util;

import com.badlogic.gdx.utils.viewport.Viewport;
import com.oogames.tapper.Ref;

public final class Utils {

	private Utils() {}
	
	public enum Proportion{
		HEIGHT,
		WIDTH,
		BOTH;
	}
	
	public static final float propHeight(float val, Viewport view){
		return applyProportions(val, view, Proportion.HEIGHT);
	}
	
	public static final float propWidth(float val, Viewport view){
		return applyProportions(val, view, Proportion.WIDTH);
	}

	public static final float applyProportions(float val, Viewport view, Proportion prop){
		int hRatio = view.getViewportHeight()/Ref.PREF_HEIGHT_PX;
		int wRatio = view.getViewportHeight()/Ref.PREF_WIDTH_PX;
		
		if(prop == Proportion.HEIGHT)
			return val * hRatio;
		else if(prop == Proportion.WIDTH)
			return val * wRatio;
		else
			return val;
	}
	
}
