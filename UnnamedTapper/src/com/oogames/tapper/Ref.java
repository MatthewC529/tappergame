package com.oogames.tapper;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.treehouseelite.elitegdx.util.Colour;
import com.treehouseelite.elitegdx.util.Referenceable;

public final class Ref implements Referenceable{
	
	private Ref(){}
	
	public static final String TITLE = "Tapper";
	public static final String VERSION = "Pre-Alpha";
	public static final String NAME = "Object Oriented Games";
	public static final String ACRONYM = "OOGames";
	
	public static final Colour BG_COLOR = new Colour("4099FF");
	
	//ASSETS
	
	public static final String TITLE_TEX = "data/mainmenu/titleBar.png",
							   ITEM1_TEX = "data/mainmenu/mItem1.png",
							   ITEM2_TEX = "data/mainmenu/mItem2.png",
							   ITEM3_TEX = "data/mainmenu/mItem3.png",
							   ITEM4_TEX = "data/mainmenu/mItem4.png",
							   CUBIT_TEX = "data/bg/cubit-large.png",
							   SMALL_CUBIT_TEX = "data/bg/cubit.png";
	
	public static final String MAIN_FNT = "data/mainmenu/fonts/continuum.fnt",
							   TITLE_FNT = "data/mainmenu/fonts/TitleFont.fnt";
	
	public static final String TITLE_MUSIC = "data/mainmenu/music/main.mp3";
	
	//--
	
	public static final int PREF_WIDTH_PX = 480;
	public static final int PREF_HEIGHT_PX = 800;
	
	public static final Map<String, String[]> CONTRIB = new HashMap<String, String[]>();
	public static String contributors;
	
	//-- METHODS --//
	@Override
	public String getAuthor() {
		if(contributors == null){
			setContributors();
			StringBuilder contrib = new StringBuilder();
			String[] arr = CONTRIB.keySet().<String>toArray(new String[CONTRIB.keySet().size()]);
			
			for(String key: arr){
				for(String s: CONTRIB.get(key)){
					if(s == CONTRIB.get(key)[CONTRIB.get(key).length-1] && key == arr[arr.length-1]) contrib.append(s);
					else contrib.append(s + ", ");
				}
			}
			
			contributors = contrib.toString();
		}
		
		return contributors;
	}

	@Override
	public String getAffiliation() {
		return NAME;
	}

	@Override
	public String getProjectName() {
		return TITLE;
	}

	@Override
	public String getVersion() {
		return VERSION;
	}

	@Override
	public String getContributors() {
		return getAuthor();
	}

	@Override
	public File getLogDirectory() {
		String dir;
		if(Gdx.app.getType() == ApplicationType.Android)
			dir = Gdx.files.getExternalStoragePath() + "/" + ACRONYM +"/" + TITLE +"/logs";
		else{
			String OS = System.getProperty("os.name").toLowerCase();
			if(OS.contains("win"))
				dir = Gdx.files.getExternalStoragePath() + "/My Documents/" + ACRONYM +"/" + TITLE + "/logs";
			else if(OS.contains("linux") || OS.contains("nix"))
				dir = Gdx.files.getExternalStoragePath() + "/Documents/" + ACRONYM + "/" + TITLE + "/logs";
			else{
				dir = Gdx.files.getExternalStoragePath() + "/Games/" + ACRONYM + "/" + TITLE + "/logs";
			}
		}
		return Gdx.files.absolute(dir).file();
 	}

	@Override
	public boolean isPlayerActive() {
		return true;
	}

	private void setContributors(){
		FileHandle fh = Gdx.files.getFileHandle("data/CONTRIB.dat", FileType.Internal);
		if(!fh.exists()){
			System.out.println(fh.path());
			System.exit(-1);
		}
		
		try(Scanner scan = new Scanner(fh.readString())){		
			while(scan.hasNextLine()){
				String line = scan.nextLine();
				String start = line.substring(0, 2);
				line = line.substring(line.indexOf(" ")+1);
				String[] names = line.split(",");
				
				if(start.equals("P:")){
					CONTRIB.put("Programmer", names);
				}else if(start.equals("A:")){
					CONTRIB.put("Artist", names);
				}else if(start.equals("D:")){
					CONTRIB.put("Designer", names);
				}
			}
		}
	}

	@Override
	public String getCopyright() {
		return "";
	}
	
}
